FROM tomcat:8.5.43-jdk11-adoptopenjdk-openj9

RUN  mkdir -p /usr/local/tomcat/appraw \
     && mkdir -p /usr/local/tomcat/webapps \
     && mkdir -p /usr/local/tomcat/logs \
     && mkdir -p /usr/local/tomcat/lib \
     && chmod -R 777 /usr/local/tomcat \
     && chown 1000:1000 -R /usr/local/tomcat

# Add copy jar files to /usr/local/tomcat/webapps/ file.war and /usr/local/tomcat/lib/ file.jar
COPY app/build/libs/app.jar /usr/local/tomcat/lib/app.jar

EXPOSE 1098
EXPOSE 1099
EXPOSE 8080

# Using jdk as base image
#FROM rmohr/activemq:5.15.4


#EXPOSE 8162
#EXPOSE 61617